﻿

init:

    $ You = Character("You")
    $ Gordon = Character("Dr. Freeman")

    image bg black mesa:
        "blackmesaeast.jpg"
        zoom 2
    image Gordon angry:
        "Gordon_Sprite_Angry_Arm_Down.png"
        zoom 0.4
    image Gordon looking away arms crossed:
        "Gordon_Sprite_Arms_Crossed_Looking_Away_Angry.png"
        zoom 0.4
    image Gordon joy:
        "Gordon_Sprite_Joy_.png"
        zoom 0.4
    image Gordon looking away angry:
        "Gordon_Sprite_Looking_Away_Angry.png"
        zoom 0.4
    image Gordon looking away blush:
        "Gordon_Sprite_Looking_Away_Blushing.png"
        zoom 0.4
    image Gordon neutral arm down:
        "Gordon_Sprite_Neutral_Arm_Down.png"
        zoom 0.4
    image Gordon neutral:
        "Gordon_Sprite_Neutral.png"
        zoom 0.4
    image Gordon shock:
        "Gordon_Sprite_Shock.png"
        zoom 0.4
    image Gordon smug blush:
        "Gordon_Sprite_Smug_Blushing.png"
        zoom 0.4
    image Gordon smug:
        "Gordon_Sprite_Smug.png"
        zoom 0.4


label start:
    #stop music
    #play music "soft_morning_breeze_loop.mp3" fadein 2

    scene bg black mesa

    "It's a peaceful evening at Black Mesa East."
    "You're waiting for the arrival of Gordon. He's supposed to be here any minute."
    "You don't want to let anyone down, which makes you feel anxious."
    "Oh! He's here!"

    show Gordon neutral arm down
    You "Welcome Dr. Freeman!"
    show Gordon neutral
    Gordon "....??"
    Gordon "Who are you?"
    "He doesn't seem to know why I'm here..."
    You "I'm the newest member of Black Mesa East."
    show Gordon neutral arm down
    Gordon "Oh? I hadn't been informed."

    "You should introduce yourself to him before doing anything."
    "But a part of you is also curious to get to know him."
    "So far, you've only heard stories about Gordon single handedly going through Black Mesa to save everyone."

    You "So..."

    menu:
        "Can you tell me about yourself?":
            jump ask

        "Please let me know if you need anything, sir.":
            jump help


    label ask:

        show Gordon shock
        Gordon "...."
        show Gordon looking away angry
        Gordon "...I'm not here to chit-chat."
        show Gordon angry
        Gordon "Why are you here, anyway?"

        return


    label help:
        show Gordon shock
        Gordon "Well, this is new."
        show Gordon looking away blush
        Gordon "People usually ask me for things, not the other way around."
        You "I'll be your helping hand around here. Eli has assigned me to assist you with anything you need."
        show Gordon joy
        Gordon "Well, I'm very happy to hear this."
        "You smile bashfully at him."
        You "I won't let you down, sir."

return
